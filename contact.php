<?php /* Template Name: TemplateContact */ ?>

<?php get_header(); ?>

	<div id="mediaField">
		<div id="singleImageArticle">
			<img src="<?php bloginfo('template_directory')?>/img/search.jpg">
			<div id="singleTitleArticle">
				<h1>Formularz kontaktowy</h1>
			</div>
		</div>
	</div>

	<div id="content">
	<div id="leftSide">
		<?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('contact_widgets')) : ?>
		<?php endif; ?>
	</div>
<?php get_footer(); ?>