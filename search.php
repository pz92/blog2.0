<?php get_header(); ?>

	<div id="mediaField">
		<div id="singleImageArticle">
			<img src="<?php bloginfo('template_directory')?>/img/search.jpg">
			<div id="singleTitleArticle">
				<h1>Wyszukiwarka</h1>
			</div>
		</div>
	</div>

	<div id="content">
	<div id="leftSide">
		<div class="infoText">
			<h2><?php printf( __( 'Search Results for: %s', 'twentyfifteen' ), get_search_query() ); ?></h2>
		</div>
		<?php include_once("frontPageArticles.php"); ?>
	</div>

<?php get_footer(); ?>