<?php /* Template Name: TemplateCalendar */ ?>
<?php get_header(); ?>

	<div id="mediaField">
		<div id="singleImageArticle">
			<img src="<?php bloginfo('template_directory')?>/img/search.jpg">
			<div id="singleTitleArticle">
				<h1>Kalendarz</h1>
			</div>
		</div>
	</div>

	<div id="content">
	<div id="leftSide">
    <?php
    $filterCategory = 'Kalendarz';
    $all_posts = new WP_Query( array( 'posts_per_page' => -1 ) );
    while( $all_posts->have_posts() ):
    $all_posts->the_post();
    $postcategories = get_the_category();
    if ($postcategories) {
    foreach($postcategories as $category) {
    $categoryname = $category->cat_name;
    if($categoryname == $filterCategory){
    ?>
    <div class="event" style="height: 150px; width: 100%; margin: 10px; background: red">
<!--        <div id="event-title">-->
<!--            --><?php //the_title(); ?>
<!--        </div>-->
<!--        <div id="event-content">-->
<!--		    --><?php //the_content(); ?>
<!--        </div>-->
        <div id="event-date">
		    <?php echo the_field('data_zakonczenia')?>
        </div>
    </div>
<?php
}
}
}
endwhile;

// reset the global post variable
		wp_reset_postdata();
?>
    </div>
<?php get_footer(); ?>