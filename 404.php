<?php get_header(); ?>

	<div id="mediaField">
		<div id="singleImageArticle">
			<img src="<?php bloginfo('template_directory')?>/img/404.jpg">
			<div id="singleTitleArticle">
				<h1>Coś poszło nie tak...</h1>
			</div>
		</div>
	</div>

	<div id="content">
	<div id="leftSide" style="padding-left: 1.8%">
		<div id="info404">
			<span class="fa">&#xf11a</span>
			<h1>Wybrana strona nie istnieje</h1>
			<h3>Możesz powrócić na <a href="<?php echo home_url();?>">stronę główną</a> lub skorzystać z wyszukiwarki</h3>
		</div>
	</div>

<?php get_footer(); ?>