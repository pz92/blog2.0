resizeTileArticle();
changeFontSize();
$(document).ready(function () {
    // pageWidth();
    $(document).scroll(function () {
        topBarPosition();
        hideMe();
    });

    $(window).resize(function() {
        changeFontSize();
        resizeTileArticle();
        topBarPosition();
        hideArticle();
        // pageWidth();
    });
});

$(".carousel").on("touchstart", function(event){
    var xClick = event.originalEvent.touches[0].pageX;
    $(this).one("touchmove", function(event){
        var xMove = event.originalEvent.touches[0].pageX;
        if( Math.floor(xClick - xMove) > 5 ){
            $(this).carousel('next');
        }
        else if( Math.floor(xClick - xMove) < -5 ){
            $(this).carousel('prev');
        }
    });
    $(".carousel").on("touchend", function(){
        $(this).off("touchmove");
    });
});

//Rozmiar czcionki slidera
function changeFontSize() {
    var $mediaField = $('#mediaField');
    var $title = $('#mediaField h1');
    var $text = $('#mediaField h3');

    var height = $mediaField.height();
    var fontTitle = height/10;
    var fontText = height/20;
    var lineHeightText = height/17;

    if(fontTitle > 25){
        $title.css({
            'font-size': fontTitle + 'px'
        });
    }
    else{
        $title.css({
            'font-size': '25px'
        });
    }

    $text.css({
        'font-size': fontText + 'px',
        'line-height': lineHeightText + 'px'
    });
}

/* Zmiana wysokości kafelka artykułu na stronie głównej */
function resizeTileArticle() {
    var maxHeight = -1;
    var heightImgField = -1;

    //reset height of tiles and check width of tiles
    $('.tileArticleFloat').each(function() {
        $(this).css('height', 'auto');
        heightImgField = $(this).width()/2;
    });

    var it = 0;
    //resize image field above text article in tiles
    $('.tileImageArticle').each(function() {

        $(this).css('height', heightImgField);
        // var tileImageField = $('.tileImageArticle').eq(it);
        // var img = $('.tileImageArticle img').eq(it);
        //
        // window.alert($(this).height() + ' ' + tileImageField.height() + ' ' + img.height());
        // // if(img.height() > tileImageField.height()){
        // //     img.css('height', 'auto', 'width', '100%');
        // // }
        // // else{
        // //     img.css('height', '100%', 'width', 'auto');
        // // }
        //
        // it++;
    });

    //check which one tile is the tallest and save score in maxHeight variable
    $('.tileArticleFloat').each(function() {
        maxHeight = maxHeight > $(this).height() ? maxHeight : $(this).height();
    });


    // $('.tileArticleFloat').each(function() {
    //     $(this).height(maxHeight);
    // });

    //set height to all tiles
    $('.tileArticleFloat').each(function() {
        $(this).css('height', maxHeight);
    });
}


//hover tiles powieszenie obrazków
$(".tileArticle").mouseover(function() {
    $(this).find(".tileImageArticle img").css('width', '105%', 'height', 'auto');

}).mouseout(function() {
    $(this).find(".tileImageArticle img").css('width', '100%', 'height', 'auto');
});

//hover slider
$("#slider").mouseover(function() {
    $(this).find(".sliderControl").css('opacity', '1');

}).mouseout(function() {
    $(this).find(".sliderControl").css('opacity', '0');
});;


function pageWidth() {
    var $text = $('.sliderText h3');

    var $width = $(window).width();
    $text.text($width);
}


function topBarPosition(){
    //jeśli jestem między 0 a wysokością mediaField
    if($(window).scrollTop()<$('#mediaField').height() && $(window).scrollTop()>=0) {
        $('#topBar').css({
            'position': 'fixed',
            'top': '0'
        })

        if (!$('#content').hasClass('single')) {
            $('#mediaField').css({
                'position': 'relative',
            })

            $('#content').css({
                'position': 'relative',
                'top': '0'
            })
        }

        else{
            $('#mediaField').css({
                'position': 'fixed',
                'top': '0'
            })

            $('#content').css({
                'position': 'absolute',
                'top': $('#mediaField').height() + 50 + 'px'
            })

            $('#mediaField #singleTitleArticle').css({
                'bottom': $(window).scrollTop() + 'px',
                'opacity': 1 - $(window).scrollTop()/($('#mediaField').height() - 100)
            })

            if($(window).scrollTop()<$('#mediaField').height() && $(window).scrollTop()>=0){
                $('#singleImageArticle img').css({
                    'width': 100 + $(window).scrollTop()/15 + '%'
                });
            }
        }
    }
    else if($(window).scrollTop()>$('#mediaField').height()){
        $('#topBar').css({
            'position': 'absolute',
            'top': $('#mediaField').height() + 'px'
        })
    }
}
topBarPosition();

hideArticle();
function hideArticle() {
    var it = 0;
    if($('#slider').is(':visible'))
    {
        $('.tileArticleFloat').each(function () {
                if(it<3){
                    $(this).css('display', 'none');
                }
                it++;
            });
        it=0;
    }
    else{
        $('.tileArticleFloat').each(function () {
            if(it<3){
                $(this).css('display', 'block');
            }
            it++;
        });
        it=0;
    }
}

hideMe();
function hideMe() {
    $('.hideme').each( function(i){

        var bottom_of_object = $(this).offset().top + $(this).outerHeight();
        var bottom_of_window = $(window).scrollTop() + $(window).height();

        /* If the object is completely visible in the window, fade it it */
        if( bottom_of_window > bottom_of_object ){

            $(this).animate({'opacity':'1'},500);

        }

    });
}