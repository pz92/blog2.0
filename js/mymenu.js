$(document).ready(function () {
    $('#menuPanel ul li').each(function() {
        var $this = $(this);
        if ($this.find('ul').length >= 1) {
            if(!$(this).find('ul').attr('class').match(/[0-9]/g)){
                var className = $(this).attr('id');
                $this.find('ul').addClass(className);
            }
            $this.find('a').eq(0).append('&#160<span class="glyphicon glyphicon-menu-down" style="position: absolute; top: 22px; font-size: 12px;"></span>').removeAttr('href');
            $this.click((function (e) {
                var id = $(this).attr('id');
                e.stopPropagation();
                submenuOpen(id);
            }))
        }
    });
});

$('#menuBtn').click(function() {
    controlMenu();
});

$('#menuClose').click(function() {
    controlMenu();
});

function controlMenu() {
    var menuPanel = document.querySelector('#menuPanel');

    if(!menuPanel.classList.contains('open')) {
        $('#menuPanel ul').css('opacity', '1');
        menuPanel.classList.add('open');
        $('#menuClose').css('display', 'block');
        $('html').css('overflow', 'hidden');
        $('#menuPanel').css('overflow', 'auto');
        $('#menuBtn').removeClass('fa-bars').addClass('fa-times').css('color', '#666666');
        $('#menuPanel ul li').addClass('animate');

        setTimeout(function () {
            $('#menuPanel ul li').addClass('animateReset');
        }, 1000);
    }

    else {
        $('#menuPanel ul').css('opacity', '0');
        $('#menuClose').css('display', 'none');
        $('html').css('overflow', 'auto');
        $('#menuPanel').css('overflow', 'hidden');
        $('#menuPanel ul li').removeClass('animate').removeClass('animateReset').find('ul').removeClass('open');

        setTimeout(function () {
            menuPanel.classList.remove('open');
            $('#menuPanel').scrollTop(0);
            $('#menuBtn').removeClass('fa-times').addClass('fa-bars').css('color', 'rgba(255,255,255,.9)');
        }, 200);
    }
}

function submenuOpen(id_) {
    var id = '#' + id_;
    if (!($(id).find('ul').eq(0).hasClass("open"))) {
        var lastClass = $(id).find('ul').attr('class').split(' ').pop();

        $('#menuPanel ul li').each(function () {
            var $this = this;
            if(!$($this).find('ul').hasClass(lastClass)){
                $(this).find('ul').removeClass('open');
            }
        });
        $(id).find('ul').eq(0).addClass('open');
    }
    else {
        $(id).find('ul').removeClass('open');
    }
}

$("#menuPanel").on("touchstart", function(event){
    var xClick = event.originalEvent.touches[0].pageX;
    $(this).one("touchmove", function(event){
        var xMove = event.originalEvent.touches[0].pageX;
        if( Math.floor(xClick - xMove) > 5 ){
            controlMenu();
        }
    });
    $("#menuPanel").on("touchend", function(){
        $(this).off("touchmove");
    });
});
