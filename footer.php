
<div id="rightSideBar">
	<?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('sidebar_right')) : ?>
	<?php endif; ?>
	<!--            <div style="position: absolute; width: 100%; height: 2px; background: white; margin-top: -22px"></div>-->
</div>

<footer id="footer">
    <section id="footerLogo" class="footerComponent">
        <p><h4>Wszystkie prawa zastrzeżone &copy 2017</h4></p>
    </section>
    <section id="footerCookies" class="footerComponent">
        <p><h4>Ważne: wykorzystujemy pliki cookies</h4></p>
        <p>
            Używamy informacji zapisanych za pomocą cookies i podobnych technologii m.in. w celach reklamowych i statystycznych oraz w celu dostosowania naszych serwisów do indywidualnych potrzeb użytkowników. Mogą też stosować je współpracujący z nami reklamodawcy, firmy badawcze oraz dostawcy aplikacji multimedialnych. W programie służącym do obsługi internetu można zmienić ustawienia dotyczące cookies.Korzystanie z naszych serwisów internetowych bez zmiany ustawień dotyczących cookies oznacza, że będą one zapisane w pamięci urządzenia.
        </p>
    </section>
</footer>
</div>

</div><!-- /perspective -->
<script src="<?php bloginfo('template_directory') ?>/js/myscript.js"></script>
<script src="<?php bloginfo('template_directory') ?>/js/mymenu.js"></script>
<script src="<?php bloginfo('template_directory') ?>/widgety/dropDownArticle/script.js"></script>
<script src="<?php bloginfo('template_directory') ?>/js/jquery.mobile.custom.min.js"></script>
<script id="dsq-count-scr" src="//mywebpage-1.disqus.com/count.js" async></script>
<?php wp_footer() ?>
</body>
</html>