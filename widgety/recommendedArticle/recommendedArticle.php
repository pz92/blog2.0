<?php
class Recommended_Article_Widget extends WP_Widget {

	public function __construct() {
		$widget_ops = array(
			'classname' => 'widget_recommendedArticle',
			'description' => __( 'Artykuły polecane czytelnikom do przeczytania.' ),
			'customize_selective_refresh' => true,
		);
		parent::__construct('recommendedarticle', __('Polecane artykuły'), $widget_ops);
	}

	public function widget( $args, $instance ) {
		if(empty($filterCategory)){
		    $filterCategory = 'Polecane';
        }
        else{
	        $filterCategory = $instance['filterCategory'];
        }

		//tytuł archiwa (podany w wpadmin)
		$title = apply_filters( 'widget_title', empty( $instance['title'] ) ? __( 'Polecane artykuły' ) : $instance['title'], $instance, $this->id_base );

		echo $args['before_widget'];
		if ( $title ) {
			echo $args['before_title'] . $title . $args['after_title'];
		}

		$recommendedarticle_id = "{$this->id_base}-{$this->number}";

		$all_posts = new WP_Query( array( 'posts_per_page' => -1 ) );
		while( $all_posts->have_posts() ):
			$all_posts->the_post();
			$postcategories = get_the_category();
			if ($postcategories) {
				foreach($postcategories as $category) {
					$categoryname = $category->cat_name;
					if($categoryname == $filterCategory){
						?>
                        <link href="<?php bloginfo('template_directory'); ?>/widgety/recommendedArticle/style.css" rel="stylesheet" type="text/css">
                        <div id="<?php esc_attr($recommendedarticle_id) ?>" class="recommendedArticle">
                            <a href="<?php the_permalink(); ?>">
								<?php $urlImage = wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'thumbnail' ); ?>
                                <div class="recommendedArticle-date "><span class="glyphicon glyphicon-time" aria-hidden="true"></span> <?php echo get_the_date('', $post->ID); ?></div>
                                <div id="recommendedArticle-title"><?php echo get_the_title($post->ID) ?></div>
                                <img class="recommendedArticle-image" src="<?php echo $urlImage ?>" alt="obrazek">
                            </a>
                        </div>
						<?php
					}
				}
			}
		endwhile;

		// reset the global post variable
		wp_reset_postdata();

		?>
        <?php

		echo $args['after_widget'];
	}

	/**
	 * Handles updating settings for the current Archives widget instance.
	 *
	 * @since 2.8.0
	 * @access public
	 *
	 * @param array $new_instance New settings for this instance as input by the user via
	 *                            WP_Widget_Archives::form().
	 * @param array $old_instance Old settings for this instance.
	 * @return array Updated settings to save.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$new_instance = wp_parse_args( (array) $new_instance, array( 'title' => '', 'filterCategory' => '') );
		$instance['title'] = sanitize_text_field( $new_instance['title'] );
		$instance['filterCategory'] = sanitize_text_field( $new_instance['filterCategory'] );

		return $instance;
	}


	//to co będzie widoczne w wp-admin w widzetach
	public function form( $instance ) {
		$instance = wp_parse_args( (array) $instance, array( 'title' => '', 'filterCategory' => '') );
		$title = sanitize_text_field( $instance['title'] );
		$filterCategory = sanitize_text_field( $instance['filterCategory'] );

		?>
		<p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label> <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" /></p>
        <p>
            <label for="<?php echo $this->get_field_id('filterCategory'); ?>"><?php _e('Nazwa kategorii, która pozwoli filtrować artykuły (domyślnie \'Polecane\'):'); ?></label> <input class="widefat" id="<?php echo $this->get_field_id('filterCategory'); ?>" name="<?php echo $this->get_field_name('filterCategory'); ?>" type="text" value="<?php echo esc_attr($filterCategory); ?>" />
        </p>
		<?php
	}
}
