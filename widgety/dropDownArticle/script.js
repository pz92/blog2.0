/* Archiwum lista dropdown */
var button = document.querySelector('.archive-button');
var dropdown = document.querySelector('.value-list');

button.addEventListener('click', function () {
    if(!dropdown.classList.contains('open')){
        dropdown.classList.add('open');
    }
    else {
        dropdown.classList.remove('open');
    }
});

document.addEventListener('click', function (evt) {
    var isDropdown = dropdown.contains(evt.target);
    var isInput = button.contains(evt.target);
    if (!isDropdown && !isInput) {
        dropdown.classList.remove('open');
    }
});

$(document).ready(function () {
    $(dropdown).css('width', ($(button).css('width')));

    // pageWidth();
    $(window).resize(function() {
        $(dropdown).css('width', ($(button).css('width')));
    });
});