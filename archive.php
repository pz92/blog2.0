<?php get_header(); ?>

	<div id="mediaField">
		<div id="singleImageArticle">
			<img src="<?php bloginfo('template_directory')?>/img/archive.jpg">
			<div id="singleTitleArticle">
				<h1>Archiwum</h1>
			</div>
		</div>
	</div>

	<div id="content">
	<div id="leftSide">
		<div class="infoText">
			<h1><?php echo get_the_date('F Y'); ?></h1>
		</div>
		<?php include_once("frontPageArticles.php"); ?>
	</div>

<?php get_footer(); ?>