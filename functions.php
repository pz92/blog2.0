<?php
add_theme_support( 'post-thumbnails' );

function register_my_menus(){
	register_nav_menus(
		array('header-menu' => __('menu_glowne'))
	);
}

add_action('init', 'register_my_menus');

//sidebar z widzetami
if(function_exists('register_sidebar'))
	register_sidebar(
		array(
			'name' => 'Prawy sidebar',
			'id' => 'sidebar_right',
			'before_widget' => '<div class="widget">',
			'after_widget' => '</div>',
			'before_title' => '<h4 style="position: relative">',
			'after_title' => '</h4>',
			'before_icon' => '&#160&#160<span style="position: absolute; top: 50%; transform: translateY(-50%); font-size: 16px;" class="fa ',
			'after_icon' => '"></span></h4>'
		)
	);

function archiwum_init()
{
	require get_template_directory() . '/widgety/dropDownArticle/dropdownArchive.php'; //do określenia ścieżki dodatkowo używamy funkcji get_template_directory(); //wczytujemy plik z zapisanym kodem widgetu przy pomocy konstrukcji require;
	register_widget( 'DropDown_Widget_Archives' );//rejestrujemy widget funkcją register_widget(), pierwszy argument tej funkcji to nazwa klasy naszego nowego widgetu, czyli ta, którą ustaliliśmy wcześniej, w tym przypadku Widget_Archiwum.
}

add_action( 'widgets_init', 'archiwum_init' );//Na koniec dajemy znać WordPress-owi (specjalna funkcja WP – add_action), kiedy nasza funkcja ma być wywoływana (widgets_init – czyli przy inicjalizacji widgetów). Jako drugi argument podajemy nazwę funkcji, którą przed chwilą zapisaliśmy najnowsze_posty_init. Uwaga na nazwy!

function tag_cloud_filter($args = array()) {
	$args['smallest'] = 10;
	$args['largest'] = 10;
	$args['unit'] = 'pt';
	return $args;
}
add_filter('widget_tag_cloud_args', 'tag_cloud_filter', 90);

//add_action('acf/register_fields', 'my_register_fields');
//
//function my_register_fields()
//{
//	require get_template_directory() . '/plugins/acf-date_time_picker/acf-date_time_picker.php';
//}

//sidebar z widzetami
if(function_exists('register_sidebar'))
	register_sidebar(
		array(
			'name' => 'Contact widgets',
			'id' => 'contact_widgets',
			'before_widget' => '<div class="widget">',
			'after_widget' => '</div>',
			'before_title' => '<h4 style="position: relative">',
			'after_title' => '</h4>'
		)
	);