<div id="slider">
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!--                 Indicators-->
        <!--                <ol class="carousel-indicators">-->
        <!--                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>-->
        <!--                    <li data-target="#myCarousel" data-slide-to="1"></li>-->
        <!--                    <li data-target="#myCarousel" data-slide-to="2"></li>-->
        <!--                </ol>-->

        <!-- Wrapper for slides -->
        <div class="carousel-inner">
	        <?php
	        // the query
	        $the_query = new WP_Query( array(
		        'posts_per_page' => 3,
	        ));
	        ?>
	        <?php if ( $the_query->have_posts() ) :
                $it=0;
	        ?>
		        <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                <a href="<?php the_permalink(); ?>">
                <? if($it == 0): ?>
                    <div class="item active">
		        <? else: ?>
                    <div class="item">
                <? endif;
                $it++;
                ?>
                <div class="sliderImage">
                    <?php the_post_thumbnail('post-thumbnail', ['class' => 'img-responsive responsive--full', 'title' => 'Feature image']); ?>
                </div>
                <div class="sliderText">
                    <h1 class="animated fadeInDown"><?php the_title(); ?></h1>
                    <h3 class="animated fadeIn">
	                    <?php
	                    $content = implode(" ", array_slice(explode(" ", get_the_content()), 0, 40));
	                    echo $content;
	                    ?>
                    </h3>
                </div>
                </a>
            </div>
            <?php endwhile; ?>
	        <?php wp_reset_postdata(); ?>
	        <?php else : ?>
                <p><?php __('No News'); ?></p>
	        <?php endif; ?>
<!--            <div class="item">-->
<!--                <div class="sliderImage">-->
<!--                    <img src="--><?php //bloginfo('template_directory')?><!--/img/2.jpg" alt="Chania">-->
<!--                </div>-->
<!--                <div class="sliderText">-->
<!--                    <h1 class="animated fadeInDown">Garść statystyk po gp singapuru 2017</h1>-->
<!--                    <h3 class="animated fadeIn">Etiam ullamcorper. Suspendisse a pellentesque dui, non felis. Maecenas malesuada elit lectus felis, malesuada ultricies. Curabitur et ligula. Ut molestie a, ultricies porta urna. Vestibulum commodo volutpat a, convallis ac, laoreet enim. Phasellus fermentum in, dolor. Pellentesque facilisis. Nulla imperdiet sit amet magna.</h3>-->
<!--                </div>-->
<!--            </div>-->
<!---->
<!--            <div class="item">-->
<!--                <div class="sliderImage">-->
<!--                    <img src="--><?php //bloginfo('template_directory')?><!--/img/3.jpg" alt="Chania">-->
<!--                </div>-->
<!--                <div class="sliderText">-->
<!--                    <h1 class="animated fadeInDown">Los Angeles</h1>-->
<!--                    <h3 class="animated fadeIn">Etiam ullamcorper. Suspendisse a pellentesque dui, non felis. Maecenas malesuada elit lectus felis, malesuada ultricies. Curabitur et ligula. Ut molestie a, ultricies porta urna. Vestibulum commodo volutpat a, convallis ac, laoreet enim. Phasellus fermentum in, dolor. Pellentesque facilisis. Nulla imperdiet sit amet magna.</h3>-->
<!--                </div>-->
<!--            </div>-->
        </div>

        <!-- Left and right controls -->
        <div id="leftControl" class="sliderControl">
            <a class="sliderButton" href="#myCarousel" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
            </a>
        </div>
        <div id="rightControl" class="sliderControl">
            <a class="sliderButton" href="#myCarousel" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
            </a>
        </div>
    </div>
</div>