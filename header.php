<!DOCTYPE html>
<html lang="pl">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width" />

	<title>Blog2.0</title>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


	<link rel="shortcut icon" href="../favicon.ico">
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory') ?>/css/animate.css"/>
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory') ?>/widgety/dropDownArticle/style.css"/>
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_url') ?>"/>

	<?php wp_head(); ?>

</head>
<body>
<div id="page">
	<div id="topBar">
		<button id="menuBtn" class="fa fa-bars"></button>
		<div id="menuPanel" class="menuPanel">
			<?php wp_nav_menu(array('header-menu' => 'menu_glowne')); ?>
		</div>
		<div id="menuClose"></div>
        <a href="<?php echo home_url(); ?>">
        <div id="logo">
<!--            <p>FORCE1</p>-->
            <img src="<?php bloginfo('template_directory'); ?>/img/logo.svg"/>
        </div>
        </a>
	</div>