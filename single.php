<?php get_header(); ?>

	<div id="mediaField">
		<div id="singleImageArticle">
			<?php
			the_post();
			the_post_thumbnail();
			?>
			<div id="singleTitleArticle">
				<h1 class="animated fadeInDown"><?php the_title(); ?></h1>
			</div>
		</div>
	</div>
	<div id="content" class="single">
	<div id="leftSide" style="padding-left: 1.8%">
		<div id="singleContent">
			<div id="singleTextArticle">
				<?php
				the_content();
				?>
			</div>
			<section id="article-info">

				<p><span style="line-height: normal" class="fa fa-folder-open"></span> Kategoria:
					<?php $postcategories = get_the_category();
					if ($postcategories) {
						foreach($postcategories as $category) {
							$categoryname = $category->cat_name;
							if($categoryname != 'Polecane') {
								?>
								<a href="<?php echo get_category_link( get_cat_ID( $categoryname ) ); ?>"><?php echo $categoryname; ?></a>
								<?php
							}
						}
					} ?>
				</p>
				<p><span style="line-height: normal" class="fa fa-tags"></span> Tagi:
					<?php
					$posttags = get_the_tags();
					if ($posttags) {
						foreach($posttags as $tag) {
							?>
							<a href="<?php echo get_tag_link($tag->term_id); ?>"><?php echo $tag->name ?>&nbsp</a>
							<?php
						}
					} ?>
				</p>
			</section>
            <section id="post-nav">
	            <?php
                $prev_post = get_previous_post();
	            if (!empty( $prev_post )): ?>
                        <div id="post-previous" class="controlArticle">
                            <a href="<?php echo $prev_post->guid ?>">
                                <h3>Poprzedni artykuł</h3>
                                <?php echo $prev_post->post_title ?>
                            </a>
                        </div>
	            <?php endif ?>

	            <?php
	            $next_post = get_next_post();
	            if (!empty( $next_post )): ?>
                    <div id="post-next" class="controlArticle">
                        <a href="<?php echo $next_post->guid ?>">
                            <h3>Następny artykuł</h3>
                            <?php echo $next_post->post_title ?>
                        </a>
                    </div>
	            <?php endif ?>
            </section>
			<div id="comments">
                <?php comments_template() ?>
			</div>
		</div>

	</div>

<?php get_footer(); ?>