<form method="get" id="searchform" action="<?php echo esc_url(home_url('/')); ?>">
	<input type="search" id="search-textfield" class="fa fa-pencil" name="s" placeholder=""/>
	<button type="submit" id="search-btn" class="fa fa-search" name="submit" value="&#xf002"></button>
</form>