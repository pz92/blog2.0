<?php get_header(); ?>

	<div id="mediaField">
		<div id="singleImageArticle">
			<img src="<?php bloginfo('template_directory')?>/img/tags.jpg">
			<div id="singleTitleArticle">
				<h1>Tagi</h1>
			</div>
		</div>
	</div>

	<div id="content">
	<div id="leftSide">
		<div class="infoText">
            <h2>Wpisy posiadające tag: <?php single_tag_title(); ?></h2>
		</div>
		<?php include_once("frontPageArticles.php"); ?>
	</div>

<?php get_footer(); ?>