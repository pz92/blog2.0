<?php if(have_posts()) : while (have_posts()) : the_post(); ?>
	<div class="tileArticleFloat">
		<div class="tileArticle">
			<a href="<?php the_permalink()?>">
				<div class='tileImageArticle'>
					<?php the_post_thumbnail(); ?>
					<div class="tileDateArticle">
						<span class="glyphicon glyphicon-time" aria-hidden="true"></span> <?php the_date(); ?>
					</div>
					<div class="tileTitleArticle">
						<h3><?php the_title(); ?></h3>
					</div>
				</div>
				<div class="tileTextArticle">
					<?php
					$content = implode(" ", array_slice(explode(" ", get_the_content()), 0, 70));
					echo $content;
					?>
				</div>
				<div class="tileCommentsArticle">
                    <span class="fa fa-comments">&#160</span><a href="<?php the_permalink(); echo '#disqus_thread'; ?>"></a>
                </div>
			</a>
		</div>

	</div>
<?php endwhile; else: ?>
    <div class="infoText">
        <h3><?php _e("Brak wpisów") ?></h3>
    </div>
<?php endif;?>